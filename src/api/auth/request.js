import axios from "axios";

const baseURL =
  process.env.NODE_ENV === "production"
    ? "http://localhost:8888/"
    : "/";

const request = axios.create({
  baseURL
});

export default request;
