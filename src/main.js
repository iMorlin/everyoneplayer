import Vue from "vue";
import App from "./App.vue";

import router from "./router";
import store from "./store";

import "./directives";
import "./filters";
import "optiscroll";
import "vue-awesome/icons";

Vue.component("icon", Icon);
Vue.component("vueSlider", vueSlider);

Vue.use(VModal);
Vue.use(VueMoment);
Vue.use(VueLazyload);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount("#app");
